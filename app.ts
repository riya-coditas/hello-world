import express from "express";

export const startServer = async () => {
    try{
        const app = express();
        const { PORT } = process.env

        app.get('/',(req,res,next)=>{
            res.send("Hello World .. Welcome to Coditas. edited");
        })

        app.listen(
            PORT || 3000,
            () => console.log(`PORT STARTED ON PORT ${PORT || 3000}`)
        )        
    }catch(e){

        console.log("COULD NOT START THE SERVER");
        console.log(e);
        process.exit(1)
    }
}
